package com.example.myapplication.slice;

import com.example.myapplication.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    Button button;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        //1,找到按钮 根据ID
         button = (Button) findComponentById(ResourceTable.Id_but1);

        //2.给按钮添加点击事件
        button.setClickedListener(this);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        if (component==button){
            //跳转到的页面（意图）
            Intent intent=new Intent();
            //包含了要跳转页面的信息
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")//要跳转的设备，空为本机
                    .withBundleName("com.example.myapplication")//跳转的应用，包名
                    .withAbilityName("com.example.myapplication.MainAbility2")//跳转的页面
                    .build();
            //operation设置到意图
            intent.setOperation(operation);
            //跳转页面
            startAbility(intent);
        }
    }
}
