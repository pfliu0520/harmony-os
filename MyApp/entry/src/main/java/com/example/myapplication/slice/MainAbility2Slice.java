package com.example.myapplication.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;

public class MainAbility2Slice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
//        super.setUIContent(ResourceTable.Layout_ability2);
        //1.创建布局对象
        DirectionalLayout directionalLayout=new DirectionalLayout(this);
        //2.创建文本对象
        Text text=new Text(this);
        text.setText("你好，夏尔！");
        text.setTextSize(70);
        text.setTextColor(Color.GRAY);
        //3.添加对象到布局中
        directionalLayout.addComponent(text);
        //4.把布局添加到子界面
        super.setUIContent(directionalLayout);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
